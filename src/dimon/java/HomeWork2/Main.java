package dimon.java.HomeWork2;

public class Main {

    public static void main(String[] args) {

        int a, b, c, sum;
        sum = 0;
        a=1;
        b=5;
        c=2;

        if(a<b&&a<c){
            sum = (int) (Math.pow(b, 2)+ Math.pow(c, 2));
            System.out.println("b+c= "+ sum);
        }
        else if(b<a&&b<c){
            sum = (int) (Math.pow(a, 2)+ Math.pow(c, 2));
            System.out.println("a+c= "+ sum);
        }
        else{
            sum = (int) (Math.pow(a, 2)+ Math.pow(b, 2));
            System.out.println("a+b= "+ sum);
        }//Задача о сумме квадратов двух наибольших чисел


        int discriminant = (int)Math.pow(b,2)- 4*a*c;
        System.out.println("\nДискриминант равен: "+discriminant);

        if(discriminant<0){
            System.out.println("Уравнение не имеет решения");
        }
        else if(discriminant==0){
            int x1 = -1*b/2*a;
            System.out.println("x1=x2="+x1);
        }
        else{
            int x1, x2;
            x1=(int) (-1*b+Math.sqrt(discriminant))/2*a;
            x2=(int) (-1*b-Math.sqrt(discriminant))/2*a;
            System.out.println("x1= "+x1+"\nx2= "+x2+"\n");
        }//Задача о решении квадратного уравнения


        String str = new String("Рвать!\" Он им: \"Я - минотавр!\"");
        System.out.println(str);
        str=str.toLowerCase();
        str=str.replace(",","");
        str=str.replace("\"","");
        str=str.replace(" -","");
        str=str.replace(":","");//Очистка предложения от внутренней пунктуации

        int baseLength = str.length();
        str=str.replace(".","");
        str=str.replace("?","");
        str=str.replace("!","");//Очистка предложения от остальной пунктуации

        int sentenceCount = baseLength-str.length();
        System.out.println("Количество предложений: "+sentenceCount);

        str=str.replace(" ","");
        int wordCount = baseLength-sentenceCount - str.length()+1;
        System.out.println("Количество слов: "+wordCount);//Нахождение количества слов и предложений


        StringBuilder stringBuilder;
        str=str.replace("ь","");//Последнее редактирование строки перед проверкой
        stringBuilder = new StringBuilder(str);
        boolean palindrom = str.equals(stringBuilder.reverse().toString());
        if(palindrom){
            System.out.println("Данное предложение является палиндромом");
        }
        else {
            System.out.println("Данное предложение не является палиндромом.");

        }//Проверка предложения на палиндромность
    }
}
